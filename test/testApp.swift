//
//  testApp.swift
//  test
//
//  Created by Sofia Sobko on 21.12.2023.
//

import SwiftUI

@main
struct testApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
