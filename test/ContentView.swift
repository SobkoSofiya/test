//
//  ContentView.swift
//  test
//
//  Created by Sofia Sobko on 21.12.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack(){
            Image("swift")
                .resizable()
                .frame(width: 100, height: 100)
                .clipShape(Circle())
                .overlay(Circle().stroke(lineWidth: 2))
            Text("Я изучаю swift")
                .foregroundColor(.orange)
            HStack(){
                Text("Лево")
                    .foregroundColor(.blue)
                Text("Право")
                    .foregroundColor(.red)
            }
            Spacer()
            
            Button {
                
            } label: {
                
            }

        }
    }
}

#Preview {
    ContentView()
}
